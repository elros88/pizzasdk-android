#PIZZA SDK

This is the **Pizza SDK**, an Android Library that will let you display a Pizza Store in your own app.

##How it works

This Library loads an Activity that shows a **Pizza Flavor Menu** that allow the user to choose up
to two pizza flavors an calculate the pizza price.

##How to use it

To use the **Pizza SDK** you must include the Library as a dependency for yor app as shown in this
[url](https://jitpack.io/#org.bitbucket.elros88/pizzasdk-android/d86ff16482)

First add this
lines to your root build.grade file:
```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```

Then add this lines to your app build.gradle file:

```
dependencies {
	        implementation 'org.bitbucket.elros88:pizzasdk-android:d86ff16482'
	}
```

Once done that, you have to call the **Pizza Store** anywhere in your app like this:

```
Intent intent = new Intent(getApplicationContext(), PizzaMainActivity.class);
startActivity(intent);
```

And that's all. Enjoy your Pizza Store.

##Test it

If you want to test the Library you can download an unsigned APK from [here](https://drive.google.com/open?id=1y1zQOQ_n5LoxKlZCtjME3YAMuAVcDnk2)
Or you can Download the source code from [Github](https://github.com/elros88/MozioTest).