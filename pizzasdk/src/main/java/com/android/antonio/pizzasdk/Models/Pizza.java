package com.android.antonio.pizzasdk.Models;

import java.util.ArrayList;
import java.util.List;

public class Pizza {

    private List<Flavor> flavors = new ArrayList<>();
    private float price;

    public Pizza() {
        this.price = 0;
    }

    public List<Flavor> getFlavors() {
        return flavors;
    }

    public void setFlavors(List<Flavor> flavors) {
        this.flavors = flavors;
    }

    public boolean addFlavor(Flavor flavor) {
        if (canAddFlavor()) {
            flavors.add(flavor);
            return true;
        }

        return false;
    }

    private boolean canAddFlavor() {
        return flavors.size() < 2 ? true : false;
    }

    public float getPrice() {
        for (int i = 0; i < flavors.size(); i++) {
            price += flavors.get(i).getPrice();
        }

        return flavors.size() < 2 ? price : price/2;
    }

    public boolean removeFlavor(Flavor flavor)
    {
        if(flavors.contains(flavor))
        {
            flavors.remove(flavor);
            return true;
        }

        return false;
    }

    public boolean flavorAlreadyChosen(Flavor flavor)
    {
        return flavors.contains(flavor);
    }
}
