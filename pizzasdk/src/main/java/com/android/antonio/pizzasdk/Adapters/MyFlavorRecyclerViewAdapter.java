package com.android.antonio.pizzasdk.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.antonio.pizzasdk.Fragments.FlavorFragment;
import com.android.antonio.pizzasdk.Fragments.FlavorFragment.OnListFragmentInteractionListener;
import com.android.antonio.pizzasdk.Models.Flavor;
import com.android.antonio.pizzasdk.R;

import java.util.List;


public class MyFlavorRecyclerViewAdapter extends RecyclerView.Adapter<MyFlavorRecyclerViewAdapter.ViewHolder> {

    private final List<Flavor> flavors;
    private final OnListFragmentInteractionListener mListener;
    private FlavorFragment fragment;

    public MyFlavorRecyclerViewAdapter(List<Flavor> items, OnListFragmentInteractionListener listener, FlavorFragment fragment) {
        flavors = items;
        mListener = listener;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_flavor, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.flavor = flavors.get(position);
        holder.flavorName.setText(flavors.get(position).getName());
        holder.flavorImage.setImageDrawable(fragment.getResources().getDrawable(flavors.get(position).getImage()));
        holder.flavorPrice.setText(flavors.get(position).getPrice().toString());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    if(fragment.getPizza().flavorAlreadyChosen(flavors.get(position)))
                    {
                        holder.flavor.setChecked(false);
                        holder.flavorCheck.setVisibility(View.GONE);
                        fragment.getPizza().removeFlavor(flavors.get(position));
                    }
                    else
                    {
                        if(fragment.getPizza().addFlavor(flavors.get(position)))
                        {
                            holder.flavorCheck.setVisibility(View.VISIBLE);
                            holder.flavor.setChecked(true);
                        }
                        else
                        {
                            fragment.showFlavorLimitDialog();
                        }
                    }

                    mListener.onListFragmentInteraction(holder.flavor);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return flavors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView flavorName;
        public final ImageView flavorImage;
        public final ImageView flavorCheck;
        public final TextView flavorPrice;
        public Flavor flavor;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            flavorName = (TextView) view.findViewById(R.id.flavor_name);
            flavorImage = (ImageView) view.findViewById(R.id.flavor_image);
            flavorCheck = (ImageView) view.findViewById(R.id.flavor_check);
            flavorPrice = (TextView) view.findViewById(R.id.flavor_price);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + flavorName.getText() + "'";
        }
    }
}
