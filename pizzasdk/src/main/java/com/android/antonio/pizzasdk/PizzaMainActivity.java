package com.android.antonio.pizzasdk;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.android.antonio.pizzasdk.Fragments.FlavorFragment;
import com.android.antonio.pizzasdk.Fragments.PizzaFragment;
import com.android.antonio.pizzasdk.Models.Flavor;
import com.android.antonio.pizzasdk.Models.Pizza;

public class PizzaMainActivity extends AppCompatActivity
        implements FlavorFragment.OnListFragmentInteractionListener,
        PizzaFragment.OnFragmentInteractionListener {

    private Pizza pizza = new Pizza();

    public PizzaMainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_main);

        loadPizzaFragment(FlavorFragment.newInstance());

    }

    public void loadPizzaFragment(Fragment fragment)
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.pizza_fragment, fragment);
        fragmentTransaction.commit();
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public void exitPizza()
    {
        finish();
    }

    @Override
    public void onListFragmentInteraction(Flavor item)
    {

    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }

}
