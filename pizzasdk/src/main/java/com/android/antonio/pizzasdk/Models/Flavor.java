package com.android.antonio.pizzasdk.Models;

import android.graphics.drawable.Drawable;

public class Flavor {

    private String name;
    private int image;
    private Double price;
    private boolean checked;

    public Flavor(String name, int image, Double price) {
        this.name = name;
        this.image = image;
        this.price = price;
        this.checked = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
