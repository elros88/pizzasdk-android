package com.android.antonio.pizzasdk.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.antonio.pizzasdk.Adapters.MyFlavorRecyclerViewAdapter;
import com.android.antonio.pizzasdk.Models.Flavor;
import com.android.antonio.pizzasdk.Models.Pizza;
import com.android.antonio.pizzasdk.PizzaMainActivity;
import com.android.antonio.pizzasdk.R;

import java.util.ArrayList;
import java.util.List;

public class FlavorFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private RecyclerView flavorListView;
    private Button buyButton;
    private ImageView exitButton;


    private Pizza pizza = new Pizza();
    private List<Flavor> flavors = new ArrayList<>();


    public FlavorFragment() {
    }


    public static FlavorFragment newInstance() {
        FlavorFragment fragment = new FlavorFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flavor_list, container, false);

        initFlavors();

        Context context = view.getContext();
        flavorListView = (RecyclerView) view.findViewById(R.id.flavor_list);
        buyButton = (Button) view.findViewById(R.id.buy_button);
        exitButton = (ImageView) view.findViewById(R.id.exit_button);

        flavorListView.setLayoutManager(new GridLayoutManager(context, 2));
        flavorListView.setAdapter(new MyFlavorRecyclerViewAdapter(flavors, mListener, FlavorFragment.this));

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pizza.getFlavors().size() > 0)
                {
                    ((PizzaMainActivity)getContext()).setPizza(pizza);
                    ((PizzaMainActivity)getContext()).loadPizzaFragment(PizzaFragment.newInstance());
                }
                else
                {
                    showChooseFlavorDialog();
                }
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PizzaMainActivity)getContext()).exitPizza();
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Flavor item);
    }

    public void showChooseFlavorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.error_message2)
                .setTitle(R.string.error_title);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    public void showFlavorLimitDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.error_message)
                .setTitle(R.string.error_title);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public void initFlavors()
    {
        Flavor pepperoni = new Flavor("Pepperoni", R.drawable.pepperoni, 10.0);
        Flavor onion = new Flavor("Onion", R.drawable.onion, 4.0);
        Flavor tomatoes = new Flavor("Tomatoes", R.drawable.tomatoes, 5.0);
        Flavor olives = new Flavor("Olives", R.drawable.olives, 8.0);
        Flavor pineapple = new Flavor("Pineapple", R.drawable.pineapple, 5.0);

        flavors.add(pepperoni);
        flavors.add(onion);
        flavors.add(tomatoes);
        flavors.add(olives);
        flavors.add(pineapple);
    }

}
