package com.android.antonio.pizzasdk.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.antonio.pizzasdk.Models.Pizza;
import com.android.antonio.pizzasdk.PizzaMainActivity;
import com.android.antonio.pizzasdk.R;


public class PizzaFragment extends Fragment {

    private ImageView firstFlavor;
    private ImageView secondFlavor;
    private TextView pizzaPrice;
    private Button acceptButton;
    private Button returnButton;

    private Pizza pizza = new Pizza();

    private OnFragmentInteractionListener mListener;

    public PizzaFragment() {

    }

    public static PizzaFragment newInstance() {
        PizzaFragment fragment = new PizzaFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        pizza = ((PizzaMainActivity)getContext()).getPizza();
        View view = inflater.inflate(R.layout.fragment_pizza, container, false);

        firstFlavor = (ImageView) view.findViewById(R.id.first_flavor);
        secondFlavor = (ImageView) view.findViewById(R.id.second_flavor);
        pizzaPrice = (TextView) view.findViewById(R.id.pizza_price);
        acceptButton = (Button) view.findViewById(R.id.take_pizza_button);
        returnButton = (Button) view.findViewById(R.id.return_button);

        firstFlavor.setImageDrawable(getResources().getDrawable(pizza.getFlavors().get(0).getImage()));

        if(pizza.getFlavors().size() > 1 )
        {
            secondFlavor.setImageDrawable(getResources().getDrawable(pizza.getFlavors().get(1).getImage()));
            secondFlavor.setVisibility(view.VISIBLE);
        }
        else
        {
            secondFlavor.setVisibility(view.GONE);
        }

        pizzaPrice.setText(String.valueOf(pizza.getPrice()));

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PizzaMainActivity)getContext()).exitPizza();
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PizzaMainActivity)getContext()).loadPizzaFragment(FlavorFragment.newInstance());
            }
        });

        return view;

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
